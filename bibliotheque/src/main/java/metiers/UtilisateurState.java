package metiers;

public abstract class UtilisateurState {

    Utilisateur utilisateur;

    UtilisateurState(Utilisateur utilisateur){
        this.utilisateur = utilisateur;
    }

    public abstract void valide() throws BadStateChangeException;

    public abstract void suspend() throws BadStateChangeException;

    public abstract void reactive() throws BadStateChangeException;

    public abstract void supprime()  throws BadStateChangeException;
}
