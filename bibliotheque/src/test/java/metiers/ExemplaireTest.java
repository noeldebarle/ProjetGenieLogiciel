package metiers;
import org.junit.Test;
import static org.junit.Assert.*;
import java.util.Date;

public class ExemplaireTest {

    @Test
    public void testGetId () {
        //GIVEN
        Ouvrage ouvr = new Ouvrage(789, "Les Misérables", "Victor Hugo");
        Date dat = new Date(2023, 01, 12);
        Exemplaire exem = new Exemplaire(123, ouvr, dat);
        //WHEN
        //THEN
        assertEquals(123, exem.getId());
    }

    @Test
    public void testGetOuvrage () {
        //GIVEN
        Ouvrage ouvr = new Ouvrage(789, "Les Misérables", "Victor Hugo");
        Date dat = new Date(2023, 01, 12);
        Exemplaire exem = new Exemplaire(123, ouvr, dat);
        //WHEN
        //THEN
        assertEquals(ouvr, exem.getOuvrage());
    }

    @Test
    public void testGetDateAchat () {
        //GIVEN
        Ouvrage ouvr = new Ouvrage(789, "Les Misérables", "Victor Hugo");
        Date dat = new Date(2023, 01, 12);
        Exemplaire exem = new Exemplaire(123, ouvr, dat);
        //WHEN
        //THEN
        assertEquals(dat, exem.getDateAchat());
    }
    
   @Test
   public void testGetState () {
       //GIVEN
       Ouvrage ouvr = new Ouvrage(789, "Les Misérables", "Victor Hugo");
       Date dat = new Date(2023, 01, 12);
       Exemplaire exem = new Exemplaire(123, ouvr, dat);
       //WHEN
       //THEN
       assertTrue(exem.getState() instanceof ExemplaireAttente);
   }

    @Test
    public void testChangeState () {
        //GIVEN
        Ouvrage ouvr = new Ouvrage(789, "Les Misérables", "Victor Hugo");
        Date dat = new Date(2023, 01, 12);
        Exemplaire exem = new Exemplaire(123, ouvr, dat);
        //WHEN
        exem.changeState(new ExemplaireDetruit(exem));
        //THEN
        assertTrue(exem.getState() instanceof ExemplaireDetruit);
    }
    
    @Test
    public void testValide () throws BadStateChangeException {
        //GIVEN
        Ouvrage ouvr = new Ouvrage(789, "Les Misérables", "Victor Hugo");
        Date dat = new Date(2023, 01, 12);
        Exemplaire exem = new Exemplaire(123, ouvr, dat);
        //WHEN
        exem.valide();
        //THEN
        assertTrue(exem.getState() instanceof ExemplaireRayon);
    }

    @Test
    public void testRetourRayon () throws BadStateChangeException {
        //GIVEN
        Ouvrage ouvr = new Ouvrage(789, "Les Misérables", "Victor Hugo");
        Date dat = new Date(2023, 01, 12);
        Exemplaire exem = new Exemplaire(123, ouvr, dat);
        exem.changeState(new ExemplaireEmprunte(exem));
        //WHEN
        exem.retourRayon();
        //THEN
        assertTrue(exem.getState() instanceof ExemplaireRayon);
    }

    @Test
    public void testDetruit () throws BadStateChangeException {
        //GIVEN
        Ouvrage ouvr = new Ouvrage(789, "Les Misérables", "Victor Hugo");
        Date dat = new Date(2023, 01, 12);
        Exemplaire exem = new Exemplaire(123, ouvr, dat);
        //WHEN
        exem.detruit();
        //THEN
        assertTrue(exem.getState() instanceof ExemplaireDetruit);
    }

    @Test
    public void testEmprunte () throws BadStateChangeException {
        //GIVEN
        Ouvrage ouvr = new Ouvrage(789, "Les Misérables", "Victor Hugo");
        Date dat = new Date(2023, 01, 12);
        Exemplaire exem = new Exemplaire(123, ouvr, dat);
        exem.changeState(new ExemplaireRayon(exem));
        //WHEN
        exem.emprunte();
        //THEN
        assertTrue(exem.getState() instanceof ExemplaireEmprunte);
    }
}