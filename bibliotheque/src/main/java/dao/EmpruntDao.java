package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import metiers.*;

public class EmpruntDao {
    Connection connection = null;
    PreparedStatement ptmt = null;
    ResultSet resultSet = null;

    public EmpruntDao() {

    }

    private Connection getConnection() throws SQLException {
        Connection conn;
        conn = DbConnection.getInstance().getConnection();
        return conn;
    }

    public void cree(int id_ex, int id_uti, java.util.Date date_empr, java.util.Date date_ret) {
        try {
            String queryString = "INSERT INTO emprunt(id_exemplaire, id_utilisateur, date_emprunt, date_retour) VALUES(?,?,?,?)";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setInt(1, id_ex);
            ptmt.setInt(2, id_uti);
            java.sql.Date sqlDateEmpr = new java.sql.Date(date_empr.getTime());
            ptmt.setDate(3, sqlDateEmpr);
            java.sql.Date sqlDateRet = new java.sql.Date(date_ret.getTime());
            ptmt.setDate(4, sqlDateRet);
            ptmt.executeUpdate();
            System.out.println("Emprunt ajouté");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (ptmt != null)
                    ptmt.close();
                if (connection != null)
                    connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void prolonge(int id_emprunt, java.util.Date date_prolongee) {

        try {
            String queryString = "UPDATE emprunt SET date_retour=? WHERE id_emprunt=?";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            java.sql.Date sqlDateProlongee = new java.sql.Date(date_prolongee.getTime());
            ptmt.setDate(1, sqlDateProlongee);
            ptmt.setInt(2, id_emprunt);
            ptmt.executeUpdate();
            System.out.println("L'emprunt a été prolongé'");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (ptmt != null)
                    ptmt.close();
                if (connection != null)
                    connection.close();
            }

            catch (SQLException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();

            }
        }
    }

}