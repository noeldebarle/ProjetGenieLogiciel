package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import metiers.*;

public class OuvrageDao {
    Connection connection = null;
    PreparedStatement ptmt = null;
    ResultSet resultSet = null;

    private Connection getConnection() throws SQLException {
        Connection conn;
        conn = DbConnection.getInstance().getConnection();
        return conn;
    }

    public OuvrageDao() {

    }
    public void cree(int isbn, String titre, String auteur) {
        try {
            String queryString = "INSERT INTO ouvrage(isbn, titre, auteur) VALUES(?,?,?)";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setInt(1, isbn);
            ptmt.setString(2, titre);
            ptmt.setString(3, auteur);
            ptmt.executeUpdate();
            System.out.println("Ouvrage ajouté");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (ptmt != null)
                    ptmt.close();
                if (connection != null)
                    connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void set_titre(int isbn, String titre) {

        try {
            String queryString = "UPDATE ouvrage SET titre=? WHERE isbn=?";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setString(1,titre);
            ptmt.setInt(2, isbn);
            ptmt.executeUpdate();
            System.out.println("Le titre a été modifié");
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            try {
                if (ptmt != null)
                    ptmt.close();
                if (connection != null)
                    connection.close();
            }

            catch (SQLException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();

            }
        }
    }


    public void set_auteur(int isbn, String auteur) {

        try {
            String queryString = "UPDATE ouvrage SET auteur=? WHERE isbn=?";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setString(1,auteur);
            ptmt.setInt(2, isbn);
            ptmt.executeUpdate();
            System.out.println("L'auteur a été modifié");
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            try {
                if (ptmt != null)
                    ptmt.close();
                if (connection != null)
                    connection.close();
            }

            catch (SQLException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();

            }
        }
    }
}