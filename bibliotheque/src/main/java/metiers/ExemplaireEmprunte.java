package metiers;

public class ExemplaireEmprunte extends ExemplaireState{

    ExemplaireEmprunte(Exemplaire exemplaire) {
        super(exemplaire);
    }

    public void valide() throws BadStateChangeException{
    	throw new BadStateChangeException("L'exemplaire est déjà validé");
    }

    public void retourRayon() throws BadStateChangeException{
    	this.exemplaire.changeState(new ExemplaireRayon(this.exemplaire));
    }

    public void detruit() throws BadStateChangeException {
    	this.exemplaire.changeState(new ExemplaireDetruit(this.exemplaire));
    }

    public void emprunte() throws BadStateChangeException{
    	throw new BadStateChangeException("Impossible d'emprunter un exemplaire déjà emprunté");
    }
}
