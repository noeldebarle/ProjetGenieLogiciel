# ProjetGenieLogiciel

Développement d'une application en JAVA de gestion de bibliothèque

## Sujet

Vous développerez par groupe de 2 ou 3 une application à minimum 2-tier de type CRUD. C’est-à-
dire que l’architecture de votre code sera la suivante :
- Une couche webservice « backend » qui devra répondre à des requêtes http
GET/POST/PUT/DELETE qui permettront respectivement de lire, ajouter, modifier, supprimer
des données. Attention dans la norme http, les requêtes PUT doivent être idempotente (=si
vous faite la même requête 1, 2 ou n fois le résultat devra toujours être le même)
- Une couche donnée avec une base de données. Le choix du moteur est libre mais doit être
cohérent avec votre application (SQL vs NoSQL).

## Attendus

En plus de cette architecture de base, votre application devra contenir :

Un design pattern vu en cours. Cela peut être :
- Une factory pour instancier les bonnes classes d’objet
- Une strategy pour adapter un traitement à une condition
- Un decorator pour ajouter du comportement à des objets
- Un observer pour faire une action quand un objet est mis à jour
- Une state machine pour enchainer des traitements dans un certain ordre

Vous êtes libre du design pattern, et il est acceptable que le design pattern ait l’air artificiel
et soit uniquement là pour répondre à cette demande.
Une base de donnée « complexe ». Si vous utilisez un modèle relationnel à minima 3 tables
sans compter les tables d’association. Dans le cas d’un modèle non relationnel il faut que vos
données s’y prêtent bien.

Des tests. Pour rester cohérent avec le cours, aucune couverture minimale est attendue,
mais vos tests doivent être intelligents et utiles. Vous pouvez décider de tout tester en
faisant du TDD, ou de tester la partie la plus compliquée. Attention tester les interactions
avec votre base va vous demander d’utiliser des outils spécifiques.

De quoi lancer votre application en utilisant seulement docker. Donc à minima un dockerfile
et des explications pour lancer votre application sont donc attendues. Vous pouvez choisir de
proposer également un script bash qui lance tout ou un fichier docker compose pour lancer
les multiples conteneurs de votre application.

Une explication rapide de votre application dans un fichier readme.md

## Notre application

### Description rapide

Comme suggéré, nous développons un système de gestion de bibliothèque. Nous allons utiliser le design pattern "state". Les classes principales métier de notre application seront les utilisateurs et les exemplaires.

### Diagramme d'état d'un exemplaire


![](https://mermaid.ink/img/pako:eNptUD2uwjAMvorlBQnRhbEDEhJsb4KRMFiN4UVq0ip1EAhxIM7BxTBpefzoZYm_PzvxGavGMpbYCQkvHO0j-eIwNcG6yJW4JsDPygTQky1gcHB5Bju6XZXUOwVYHtm3NWnMIFAH3bSPbcZbKIoZzEU4CPfkALKwolMTejqXmVz6NqY_-xN9-9_bLFhicvJP4EN5jfigB5AFfTFO0HP05Kzu5vywGJRf9vq3UkvLO0q1GDTholZK0qxPocJSm_AEU2tf28RyR3XHlzvZL3Wm?type=png)

### Diagramme d'état d'un compte utilisateur

![](https://mermaid.ink/img/pako:eNplkMGKAjEMhl8l5CKIc_E4h4UFj570aD2UaUYL005pE0HEB9rn8MU22hHdtZf2__8vSckFu9ERtljYMq28PWQbmtPSROczdezHCOuNiaDngYDBiQoEbnb7UVNvidCNIWku7AdflJRsEGyBsqzlu_kemuYLvpkpMlVzEjXQcf2nvZWUsg_Pgjs0-SVRdFL9p_ro9Ma_9_nD_4-qekT6a1xgoBysd7qny50xyEdSHFt9OuqtDGzQxKuiVnjcnmOHLWehBUpyr81i29uh0PUXUEh6Wg?type=png)


### Les cas d'utilisation

- **Créer un utilisateur**
- **Modifier un utilisateur**
- **Supprimer un utilisateur**
- **Créer un ouvrage**
- **Modifier un ouvrage**
- **Supprimer un ouvrage**
- **Créer un exemplaire**
- **Modifier un exemplaire**
- **Supprimer un exemplaire**
- **Créer un emprunt**
- **Prolonger un emprunt**
- **Retourner un ouvrage**
- **Lister les emprunts d'un utilisateur**

### Le diagramme de classes

![](https://mermaid.ink/img/pako:eNrFVrFOwzAQ_RXLExQYWCsWpDIwIVGxZTHxtbWU2JF9rqhK_52r7SpOUkURqDRDEr179vM5787Z89JI4HNeVsK5hRJrK-pCM7oCwj5QVcoJBG_3ET9ed0ojUzIDlmiVXjNt6iEopAXnIAssaEIm6fYsN-CU0VlsKyol4eY2g5x3DWjZwSyIEtW2T2waq2oCT1AMHgpSGCS0RHrsLyM9pvuMCPo6yjR6dQXdZZzTX0U6Ui8v3RbNyxfUTSWU7ai2pi83AseXYwGNt-9iZ3QHl4DWK-xgpGU9-WnE9O2Cpnj-L-Ij0tNs_2vxEeUw23WSfknUy6mPiC_i6P-qupTrWc-n2LnQe8g9i7zS4QLYKZHGmsro9blNO0HZQt781op1Z8_TUaTcpx6iqNDCmXPLHxtIr8Kz1sLMw8PgPBmwAsqevrvcVAxT2ce-PZF76rWT6fGjRnrrnZBcr2_0OdmMgzKfxA2Gn8Q81dEkcvJ94saRrOCzgjPiFfyRXrItGOW1s0Za8laKBtqsR4vEcOf3vAZbCyXpHyv4seC4AdpuPqdXCSvhKyx4oQ9EJcOZ5U6XfE7rh3vum2N9pL8yPl-JysHhB6e68Vo?type=png)
