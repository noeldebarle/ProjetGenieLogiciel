package metiers;

public abstract class ExemplaireState {

    Exemplaire exemplaire;

    ExemplaireState(Exemplaire exemplaire) {
        this.exemplaire = exemplaire;
    }

        public abstract void valide() throws BadStateChangeException;

        public abstract void retourRayon() throws BadStateChangeException;

        public abstract void detruit() throws BadStateChangeException;

        public abstract void emprunte() throws BadStateChangeException;
}
