package metiers;
import java.util.Date;

public class Exemplaire {

    private int id;
    private Ouvrage ouvrage;
    private Date dateAchat;
    private ExemplaireState state; //utile ?

    public Exemplaire(int id, Ouvrage ouvrage, Date dateAchat) {
        this.id = id;
        this.ouvrage = ouvrage;
        this.dateAchat = dateAchat;
        this.state = new ExemplaireAttente(this);

        dao.ExemplaireDao exdao = new dao.ExemplaireDao();
        exdao.cree(dateAchat, ouvrage.getIsbn(), 1);
    }

    public void changeState(ExemplaireState state) {
        this.state = state;

        dao.ExemplaireDao exdao = new dao.ExemplaireDao();
        if (this.state instanceof ExemplaireAttente) {
            exdao.set_etat(this.getId(), 1);
        } else if (this.state instanceof ExemplaireDetruit) {
            exdao.set_etat(this.getId(), 2);
        } else if (this.state instanceof ExemplaireEmprunte) {
            exdao.set_etat(this.getId(), 3);
        } else { // Cas exemplaire en rayon
            exdao.set_etat(this.getId(), 4);

        }
    }

    public void valide() throws BadStateChangeException {
        this.state.valide();
    }

    public void retourRayon() throws BadStateChangeException {
        this.state.retourRayon();
    }

    public void detruit() throws BadStateChangeException {
        this.state.detruit();

    }

    public void emprunte() throws BadStateChangeException {
        this.state.emprunte();
    }

    public int getId() {
        return this.id;
    }

    public Ouvrage getOuvrage() {
        return this.ouvrage;
    }

    public Date getDateAchat() {
        return this.dateAchat;
    }

    public ExemplaireState getState() {
        return this.state;
    }
}