package metiers;

public class UtilisateurSupprime extends UtilisateurState {

    UtilisateurSupprime(Utilisateur utilisateur) {

        super(utilisateur);
    }

    @Override
    public void valide() throws BadStateChangeException{
    	throw new BadStateChangeException("Impossible de valider un utilisateur supprimé.");
    }

    @Override
    public void suspend() throws BadStateChangeException {
    	throw new BadStateChangeException("Impossible de suspendre un utilisateur supprimé.");
    }

    @Override
    public void reactive() throws BadStateChangeException{
    	throw new BadStateChangeException("Impossible de réactiver un utilisateur supprimé.");
    }

    @Override
    public void supprime() throws BadStateChangeException {
    	throw new BadStateChangeException("Impossible de supprimer un utilisateur déjà supprimé.");
    }
}

