package dao;
import java.sql.*;
import java.util.Properties;

public class DbConnection{
	String driverClassName = "org.postgresql.Driver";
	String url = "jdbc:postgresql://localhost:3141/bibliotheque";
	String user = "postgres";
	String password = "singe";

	public static DbConnection dbConnection = null;

	public DbConnection() {
		try {
			Class.forName(driverClassName);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public Connection getConnection() throws SQLException {
		Connection conn = null;
		conn = DriverManager.getConnection(url, user, password);
		return conn;
	}

	public static DbConnection getInstance() {
		if (dbConnection == null) {
			dbConnection = new DbConnection();
		}
		return dbConnection;
	}
}

