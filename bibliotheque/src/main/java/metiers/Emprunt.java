package metiers;
import java.util.Date;

public class Emprunt {
	
    private static int compteur = 0;
	public final int id;
	private Utilisateur utilisateur;
	private Exemplaire exemplaire;
    private Date dateEmprunt;
    private Date dateRetour;

    public Emprunt(Utilisateur utilisateur, Exemplaire exemplaire, Date dateEmprunt, Date dateRetour) {
        this.id = compteur++;
    	this.utilisateur = utilisateur;
    	this.exemplaire = exemplaire;
    	this.dateEmprunt = dateEmprunt;
    	this.dateRetour = dateRetour;

        dao.EmpruntDao emprdao = new dao.EmpruntDao();
        emprdao.cree(exemplaire.getId(), utilisateur.getId(), dateEmprunt, dateRetour);
    }
    
    public void prolonge(Date dateProlongee) {

        dao.EmpruntDao emprdao = new dao.EmpruntDao();
        emprdao.prolonge(this.getId(), dateProlongee);
    }

    public int getId() {
        return this.id;
    }

    public Utilisateur getUtilisateur() {
        return this.utilisateur;
    }

    public Exemplaire getExemplaire() {
        return this.exemplaire;
    }

    public Date getDateEmprunt() {
        return this.dateEmprunt;
    }

    public Date getDateRetour() {
        return this.dateRetour;
    }
}
