CREATE DATABASE bibliotheque;

\c bibliotheque

CREATE TABLE etat_uti (
    id_etat_uti SERIAL PRIMARY KEY,
    nom_etat_uti VARCHAR(100) NOT NULL
    );

INSERT INTO etat_uti (nom_etat_uti) VALUES
    ('actif'),('attente'),('supprimé'),('suspendu');

CREATE TABLE utilisateur (
    id_utilisateur SERIAL PRIMARY KEY,
    nom VARCHAR(100) NOT NULL,
    adresse VARCHAR(200) NOT NULL,
    date_adhesion DATE NOT NULL,
    id_etat_uti INTEGER REFERENCES etat_uti (id_etat_uti)
    );

CREATE TABLE ouvrage (
    isbn INTEGER NOT NULL PRIMARY KEY,
    titre VARCHAR(200) NOT NULL,
    auteur VARCHAR(100) NOT NULL
    );

CREATE TABLE etat_ex (
    id_etat_ex SERIAL PRIMARY KEY,
    nom_etat_ex VARCHAR(100) NOT NULL
    );

INSERT INTO etat_ex (nom_etat_ex) VALUES
    ('attente'),('détruit'),('emprunté'),('rayon');

CREATE TABLE exemplaire (
    id_exemplaire SERIAL PRIMARY KEY,
    date_achat DATE NOT NULL,
    isbn INTEGER REFERENCES ouvrage (isbn),
    id_etat_ex INTEGER REFERENCES etat_ex (id_etat_ex)
    );

CREATE TABLE emprunt (
    id_emprunt SERIAL PRIMARY KEY,
    id_exemplaire INTEGER REFERENCES exemplaire (id_exemplaire),
    id_utilisateur INTEGER REFERENCES utilisateur (id_utilisateur),
    date_emprunt DATE NOT NULL,
    date_retour DATE NOT NULL
    );