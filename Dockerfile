FROM maven

WORKDIR /app

COPY /bibliotheque/ .

RUN mvn package

#CMD ["java", "-jar", "./target/bibliotheque-1.0-SNAPSHOT.jar"]
CMD ["java", "./src/main/java/main.java"]

#FROM maven

#COPY /bibliotheque/pom.xml /tmp/pom.xml
#RUN mvn -B -f /tmp/pom.xml -s /usr/share/maven/ref/settings-docker.xml dependency:resolve


#FROM openjdk:11
#COPY /bibliotheque/src/main/java/ /app
#WORKDIR /app
#CMD java org/example/Main
