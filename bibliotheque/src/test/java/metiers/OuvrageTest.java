package metiers;
import org.junit.Test;
import static org.junit.Assert.*;

public class OuvrageTest {

    @Test
    public void testGetIsbn () {
        //GIVEN
        Ouvrage ouvr = new Ouvrage(456, "Voyage au bout de la nuit", "Louis-Ferdinand Céline");
        //WHEN
        //THEN
        assertEquals(456, ouvr.getIsbn());
    }

    @Test
    public void testGetTitre () {
        //GIVEN
        Ouvrage ouvr = new Ouvrage(456, "Voyage au bout de la nuit", "Louis-Ferdinand Céline");
        //WHEN
        //THEN
        assertEquals("Voyage au bout de la nuit", ouvr.getTitre());
    }

    @Test
    public void testGetAuteur () {
        //GIVEN
        Ouvrage ouvr = new Ouvrage(456, "Voyage au bout de la nuit", "Louis-Ferdinand Céline");
        //WHEN
        //THEN
        assertEquals("Louis-Ferdinand Céline", ouvr.getAuteur());
    }

    @Test
    public void testSetTitre () {
        //GIVEN
        Ouvrage ouvr = new Ouvrage(456, "Voyage au bout de la nuit", "Louis-Ferdinand Céline");
        //WHEN
        ouvr.setTitre("Voyage au centre de la terre");
        //THEN
        assertEquals("Voyage au centre de la terre", ouvr.getTitre());
    }

    @Test
    public void testSetAuteur () {
        //GIVEN
        Ouvrage ouvr = new Ouvrage(456, "Voyage au bout de la nuit", "Louis-Ferdinand Céline");
        //WHEN
        ouvr.setAuteur("Céline");
        //THEN
        assertEquals("Céline", ouvr.getAuteur());
    }
}