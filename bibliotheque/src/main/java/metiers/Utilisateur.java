package metiers;
import java.sql.Date;
import dao.UtilisateurDao.*;

public class Utilisateur {

    private int id;
    private String nom;
    private String adresse;
    private Date dateAdhesion;
    private UtilisateurState state; //utile ?

    public Utilisateur(int id, String nom, String adresse, Date dateAdhesion) {
    	this.id = id;
    	this.nom = nom;
    	this.adresse = adresse;
    	this.dateAdhesion = dateAdhesion;
    	this.state = new UtilisateurAttente(this);

        dao.UtilisateurDao utidao = new dao.UtilisateurDao();
        utidao.cree(nom,adresse,dateAdhesion,2);
        // 2 comme id_etat_uti car c'est l'id de l'état attente dans la table etat_uti
    }
    
    public void changeState(UtilisateurState state) {
        this.state = state;

        dao.UtilisateurDao utidao = new dao.UtilisateurDao();
        if (this.state instanceof UtilisateurActif) {
            utidao.set_etat(this.getId(),1);
        }
        else if (this.state instanceof UtilisateurAttente) {
            utidao.set_etat(this.getId(),2);
        }
        else if (this.state instanceof UtilisateurSupprime) {
            utidao.set_etat(this.getId(),3);
        }
        else { // Cas utilisateur suspendu
            utidao.set_etat(this.getId(),4);
        }
    }

    public void valide() throws BadStateChangeException{
    	this.state.valide();
    }

    public void suspend() throws BadStateChangeException {
    	this.state.suspend();

    }

    public void reactive() throws BadStateChangeException{
    	this.state.reactive();

    }

    public void supprime() throws BadStateChangeException{
    	this.state.supprime();

    }

    public int getId() {
        return this.id;
    }

    public String getNom() {
        return this.nom;
    }

    public void setNom(String nom) {
        this.nom = nom;

        dao.UtilisateurDao utidao = new dao.UtilisateurDao();
        utidao.set_nom(this.getId(),nom);
    }

    public String getAdresse() {
        return this.adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;

        dao.UtilisateurDao utidao = new dao.UtilisateurDao();
        utidao.set_adresse(this.getId(),adresse);
    }

    public Date getDateAdhesion() {
        return this.dateAdhesion;
    }

    public UtilisateurState getState() {
        return this.state;
    }

    public void voirUtilisateurs() {
        dao.UtilisateurDao utidao = new dao.UtilisateurDao();
        utidao.listeUtilisateurs();
    }
    public void voirEmpruntsUtilisateur() {
        dao.UtilisateurDao utidao = new dao.UtilisateurDao();
        utidao.listeEmpruntsUtilisateur(this.getId());
    }
}
