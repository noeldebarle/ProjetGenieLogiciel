package metiers;
import org.junit.Test;
import static org.junit.Assert.*;
import java.sql.Date;

public class EmpruntTest {

    @Test
    public void testGetId () {
        //GIVEN
        Date dat0 = new Date(1999, 12, 31);
        Utilisateur util = new Utilisateur(369, "Rigaud", "88 avenue Verdier", dat0);
        Ouvrage ouvr = new Ouvrage(789, "Les Misérables", "Victor Hugo");
        Date dat = new Date(2001, 03, 06);
        Exemplaire exem = new Exemplaire(147, ouvr, dat);
        Date dat1 = new Date(2000, 01, 01);
        Date dat2 = new Date(2000, 01, 31);
        Emprunt empr = new Emprunt(util, exem, dat1, dat2);
        //WHEN
        //THEN
        assertNotNull(empr.getId());
    }

    @Test
    public void testGetUtilsateur () {
        //GIVEN
        Date dat0 = new Date(1999, 12, 31);
        Utilisateur util = new Utilisateur(369, "Rigaud", "88 avenue Verdier", dat0);
        Ouvrage ouvr = new Ouvrage(789, "Les Misérables", "Victor Hugo");
        Date dat = new Date(2001, 03, 06);
        Exemplaire exem = new Exemplaire(147, ouvr, dat);
        Date dat1 = new Date(2000, 01, 01);
        Date dat2 = new Date(2000, 01, 31);
        Emprunt empr = new Emprunt(util, exem, dat1, dat2);
        //WHEN
        //THEN
        assertEquals(util, empr.getUtilisateur());
    }

    @Test
    public void testGetExemplaire () {
        //GIVEN
        Date dat0 = new Date(1999, 12, 31);
        Utilisateur util = new Utilisateur(369, "Rigaud", "88 avenue Verdier", dat0);
        Ouvrage ouvr = new Ouvrage(789, "Les Misérables", "Victor Hugo");
        Date dat = new Date(2001, 03, 06);
        Exemplaire exem = new Exemplaire(147, ouvr, dat);
        Date dat1 = new Date(2000, 01, 01);
        Date dat2 = new Date(2000, 01, 31);
        Emprunt empr = new Emprunt(util, exem, dat1, dat2);
        //WHEN
        //THEN
        assertEquals(exem, empr.getExemplaire());
    }

    @Test
    public void testGetDateEmprunt () {
        //GIVEN
        Date dat0 = new Date(1999, 12, 31);
        Utilisateur util = new Utilisateur(369, "Rigaud", "88 avenue Verdier", dat0);
        Ouvrage ouvr = new Ouvrage(789, "Les Misérables", "Victor Hugo");
        Date dat = new Date(2001, 03, 06);
        Exemplaire exem = new Exemplaire(147, ouvr, dat);
        Date dat1 = new Date(2000, 01, 01);
        Date dat2 = new Date(2000, 01, 31);
        Emprunt empr = new Emprunt(util, exem, dat1, dat2);
        //WHEN
        //THEN
        assertEquals(dat1, empr.getDateEmprunt());
    }

    @Test
    public void testGetDateRetour () {
        //GIVEN
        Date dat0 = new Date(1999, 12, 31);
        Utilisateur util = new Utilisateur(369, "Rigaud", "88 avenue Verdier", dat0);
        Ouvrage ouvr = new Ouvrage(789, "Les Misérables", "Victor Hugo");
        Date dat = new Date(2001, 03, 06);
        Exemplaire exem = new Exemplaire(147, ouvr, dat);
        Date dat1 = new Date(2000, 01, 01);
        Date dat2 = new Date(2000, 01, 31);
        Emprunt empr = new Emprunt(util, exem, dat1, dat2);
        //WHEN
        //THEN
        assertEquals(dat2, empr.getDateRetour());
    }
    
    @Test
    public void testProlonge () {
        //GIVEN
        //WHEN
        //THEN
    }

    @Test
    public void testRetour () {
        //GIVEN
        //WHEN
        //THEN
    }

}