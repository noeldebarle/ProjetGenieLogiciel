package metiers;

public class UtilisateurAttente extends UtilisateurState{


    UtilisateurAttente(Utilisateur utilisateur) {
        super(utilisateur);
    }

    @Override
    public void valide() throws BadStateChangeException {
    	this.utilisateur.changeState(new UtilisateurActif(this.utilisateur));

    }

    @Override
    public void suspend() throws BadStateChangeException {
    	throw new BadStateChangeException("Impossible de suspendre un utilisateur en attente de validation.");
    }

    @Override
    public void reactive()  throws BadStateChangeException {
    	throw new BadStateChangeException("Impossible de réactiver un utilisateur en attente de validation.");
    }

    @Override
    public void supprime()  throws BadStateChangeException{
    	this.utilisateur.changeState(new UtilisateurSupprime(this.utilisateur));
    }
}
