package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import metiers.*;

public class UtilisateurDao {
    Connection connection = null;
    PreparedStatement ptmt = null;
    ResultSet resultSet = null;

    public UtilisateurDao() {

    }

    private Connection getConnection() throws SQLException {
        Connection conn;
        conn = DbConnection.getInstance().getConnection();
        return conn;
    }

    public void cree(String nom, String adresse, java.util.Date date_adhesion, int id_etat_uti) {
        try {
            String queryString = "INSERT INTO utilisateur(nom, adresse, date_adhesion, id_etat_uti) VALUES(?,?,?,?)";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setString(1, nom);
            ptmt.setString(2, adresse);
            java.sql.Date sqlDateAdhesion = new java.sql.Date(date_adhesion.getTime());
            ptmt.setDate(3,sqlDateAdhesion);
            ptmt.setInt(4,id_etat_uti);
            ptmt.executeUpdate();
            System.out.println("Utilisateur ajouté");
            
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (ptmt != null)
                    ptmt.close();
                if (connection != null)
                    connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        

    }
    public void set_etat(int id_uti, int id_etat) {

        try {
            String queryString = "UPDATE utilisateur SET id_etat_uti=? WHERE id_utilisateur=?";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setInt(1, id_etat); // 1 car id de l'état actif dans la table etat_uti
            ptmt.setInt(2, id_uti);
            ptmt.executeUpdate();
            System.out.println("L'état de l'utilisateur a été modifié");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (ptmt != null)
                    ptmt.close();
                if (connection != null)
                    connection.close();
            }

            catch (SQLException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();

            }
        }
    }

    public void set_adresse(int id_uti, String adresse) {

        try {
            String queryString = "UPDATE utilisateur SET adresse=? WHERE id_utilisateur=?";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setString(1,adresse);
            ptmt.setInt(2, id_uti);
            ptmt.executeUpdate();
            System.out.println("L'adresse a été modifiée");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (ptmt != null)
                    ptmt.close();
                if (connection != null)
                    connection.close();
            }

            catch (SQLException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();

            }
        }
    }

    public void set_nom(int id_uti, String nom) {

        try {
            String queryString = "UPDATE utilisateur SET nom=? WHERE id_utilisateur=?";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setString(1,nom);
            ptmt.setInt(2, id_uti);
            ptmt.executeUpdate();
            System.out.println("L'adresse a été modifiée");
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            try {
                if (ptmt != null)
                    ptmt.close();
                if (connection != null)
                    connection.close();
            }

            catch (SQLException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();

            }
        }
    }
    
    public void listeUtilisateurs() {
        try {
            String queryString = "SELECT * FROM utilisateur";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            resultSet = ptmt.executeQuery();
            while (resultSet.next()) {
                System.out.println("Id " + resultSet.getInt("id_utilisateur")
                        + ", Nom " + resultSet.getString("nom") + ", Adresse "
                        + resultSet.getString("adresse") + ", Date d'adhésion "
                        + resultSet.getString("date_adhesion") + ", Etat "
                        + resultSet.getInt("id_etat_uti"));
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            try {
                if (resultSet != null)
                    resultSet.close();
                if (ptmt != null)
                    ptmt.close();
                if (connection != null)
                    connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
    public void listeEmpruntsUtilisateur(int id_uti) {
        try {
            String queryString = "SELECT * FROM emprunt WHERE id_utilisateur=?";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setInt(1,id_uti);
            resultSet = ptmt.executeQuery();
            while (resultSet.next()) {
                System.out.println("Id emprunt" + resultSet.getInt("id_emprunt")
                        + ", Id exemplaire " + resultSet.getString("id_exemplaire")
                        + ", Date d'emprunt" + resultSet.getString("date_emprunt")
                        + ", Date de retour " + resultSet.getString("date_retour"));
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            try {
                if (resultSet != null)
                    resultSet.close();
                if (ptmt != null)
                    ptmt.close();
                if (connection != null)
                    connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            }
    }


}