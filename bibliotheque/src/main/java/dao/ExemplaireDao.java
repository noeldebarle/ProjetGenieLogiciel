package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import metiers.*;

public class ExemplaireDao {
    Connection connection = null;
    PreparedStatement ptmt = null;
    ResultSet resultSet = null;

    public ExemplaireDao() {

    }

    private Connection getConnection() throws SQLException {
        Connection conn;
        conn = DbConnection.getInstance().getConnection();
        return conn;
    }

    public void cree(java.util.Date date_achat, int isbn, int id_etat_ex) {
        try {
            String queryString = "INSERT INTO exemplaire(date_achat, isbn, id_etat_ex) VALUES(?,?,?)";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            java.sql.Date sqlDate = new java.sql.Date(date_achat.getTime());
            ptmt.setDate(1,sqlDate);
            ptmt.setInt(2, isbn);
            ptmt.setInt(3, id_etat_ex);
            ptmt.executeUpdate();
            System.out.println("Exemplaire ajouté");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (ptmt != null)
                    ptmt.close();
                if (connection != null)
                    connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    public void set_etat(int id_ex, int id_etat_ex) {

        try {
            String queryString = "UPDATE exemplaire SET id_etat_ex=? WHERE id_exemplaire=?";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setInt(1, id_etat_ex);
            ptmt.setInt(2, id_ex);
            ptmt.executeUpdate();
            System.out.println("L'état de l'exemplaire a été modifié");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (ptmt != null)
                    ptmt.close();
                if (connection != null)
                    connection.close();
            }

            catch (SQLException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();

            }
        }
    }

   }