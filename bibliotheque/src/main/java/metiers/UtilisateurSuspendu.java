package metiers;

public class UtilisateurSuspendu extends UtilisateurState{

    UtilisateurSuspendu(Utilisateur utilisateur) {
        super(utilisateur);
    }

    @Override
    public void valide() throws BadStateChangeException{
    	throw new BadStateChangeException("Impossible de valider un utilisateur suspendu.");
    }

    @Override
    public void suspend() throws BadStateChangeException {
    	throw new BadStateChangeException("Impossible de suspendre un utilisateur déjà suspendu.");
    }

    @Override
    public void reactive() throws BadStateChangeException{
    	this.utilisateur.changeState(new UtilisateurActif(this.utilisateur));
    }

    @Override
    public void supprime() throws BadStateChangeException {
    	this.utilisateur.changeState(new UtilisateurSupprime(this.utilisateur));
    }
}
