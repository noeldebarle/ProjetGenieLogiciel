package metiers;
import dao.*;

public class Ouvrage {
	
    private int isbn;
    private String titre;
    private String auteur;

	public Ouvrage(int isbn, String titre, String auteur) {
		this.isbn = isbn;
		this.titre = titre;
		this.auteur = auteur;

		dao.OuvrageDao ouvdao = new dao.OuvrageDao();
		ouvdao.cree(isbn, titre, auteur);
	}

	public int getIsbn() {
		return this.isbn;
	}

	public String getTitre() {
		return this.titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;

		dao.OuvrageDao ouvdao = new dao.OuvrageDao();
		ouvdao.set_titre(this.getIsbn(),titre);
	}

	public String getAuteur() {
		return this.auteur;
	}

	public void setAuteur(String auteur) {
		this.auteur = auteur;
		
		dao.OuvrageDao ouvdao = new dao.OuvrageDao();
		ouvdao.set_auteur(this.getIsbn(),auteur);
	}
}
