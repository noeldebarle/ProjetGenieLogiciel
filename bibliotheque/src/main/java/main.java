import io.vertx.core.Vertx;

import java.sql.SQLException;

import api.MyApiVerticle;
import dao.DbConnection;

public class main {

	public main() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) throws SQLException {
		
		DbConnection conn = new DbConnection();
		System.out.println("App...");
	    final Vertx vertx = Vertx.vertx();
	    vertx.deployVerticle(new MyApiVerticle());
	}

}
