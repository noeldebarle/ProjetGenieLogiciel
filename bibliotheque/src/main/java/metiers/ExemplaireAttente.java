package metiers;

public class ExemplaireAttente extends ExemplaireState{

    ExemplaireAttente(Exemplaire exemplaire) {
        super(exemplaire);
    }

    public void valide() throws BadStateChangeException{
    	this.exemplaire.changeState(new ExemplaireRayon(this.exemplaire));
    }

    public void retourRayon() throws BadStateChangeException{
    	throw new BadStateChangeException("Impossible de remettre en rayon un exemplaire en attente");
    }

    public void detruit() throws BadStateChangeException {
    	this.exemplaire.changeState(new ExemplaireDetruit(this.exemplaire));
    }

    public void emprunte() throws BadStateChangeException{
    	throw new BadStateChangeException("Impossible d'emprunter un exemplaire en attente");
    }
}
