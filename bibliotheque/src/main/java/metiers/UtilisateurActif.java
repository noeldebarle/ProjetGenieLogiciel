package metiers;

public class UtilisateurActif extends UtilisateurState{

    UtilisateurActif(Utilisateur utilisateur) {

        super(utilisateur);
    }

    @Override
    public void valide() throws BadStateChangeException{
    	throw new BadStateChangeException("Impossible de valider un utilisateur déjà actif.");
    }

    @Override
    public void suspend() throws BadStateChangeException {
    	this.utilisateur.changeState(new UtilisateurSuspendu(this.utilisateur));
    }

    @Override
    public void reactive() throws BadStateChangeException{
    	throw new BadStateChangeException("Impossible de réactiver un utilisateur déjà actif.");
    }

    @Override
    public void supprime() throws BadStateChangeException {
    	this.utilisateur.changeState(new UtilisateurSupprime(this.utilisateur));
    }
}
