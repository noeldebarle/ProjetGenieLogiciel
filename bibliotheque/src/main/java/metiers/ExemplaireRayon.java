package metiers;

public class ExemplaireRayon extends ExemplaireState{

    ExemplaireRayon(Exemplaire exemplaire) {
        super(exemplaire);
    }

    public void valide() throws BadStateChangeException {
        throw new BadStateChangeException("L'exemplaire est déjà validé");
    }

    public void retourRayon() throws BadStateChangeException{
        throw new BadStateChangeException("Impossible de remettre en rayon un exemplaire déjà en rayon");
    }

    public void detruit() throws BadStateChangeException{
        this.exemplaire.changeState(new ExemplaireDetruit(this.exemplaire));
    }

    public void emprunte() throws BadStateChangeException{
        this.exemplaire.changeState(new ExemplaireEmprunte(this.exemplaire));
    }
}
