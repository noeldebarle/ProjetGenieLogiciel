package metiers;
import org.junit.Test;
import static org.junit.Assert.*;
import java.sql.Date;

public class UtilisateurTest {

    @Test
    public void testGetId () {
        //GIVEN
        Date dat = new Date(2022, 12, 25);
        Utilisateur util = new Utilisateur(258, "Macaux", "51 rue Blaise Pascal", dat);
        //WHEN
        //THEN
        assertEquals(258, util.getId());
    }

    @Test
    public void testGetNom () {
        //GIVEN
        Date dat = new Date(2022, 12, 25);
        Utilisateur util = new Utilisateur(258, "Macaux", "51 rue Blaise Pascal", dat);
        //WHEN
        //THEN
        assertEquals("Macaux", util.getNom());
    }

    @Test
    public void testGetAdresse () {
        //GIVEN
        Date dat = new Date(2022, 12, 25);
        Utilisateur util = new Utilisateur(258, "Macaux", "51 rue Blaise Pascal", dat);
        //WHEN
        //THEN
        assertEquals("51 rue Blaise Pascal", util.getAdresse());
    }

    @Test
    public void testSetNom () {
        //GIVEN
        Date dat = new Date(2022, 12, 25);
        Utilisateur util = new Utilisateur(258, "Macaux", "51 rue Blaise Pascal", dat);
        //WHEN
        util.setNom("Bertail");
        //THEN
        assertEquals("Bertail", util.getNom());
    }

    @Test
    public void testSetAdresse () {
        //GIVEN
        Date dat = new Date(2022, 12, 25);
        Utilisateur util = new Utilisateur(258, "Macaux", "51 rue Blaise Pascal", dat);
        //WHEN
        util.setAdresse("130 avenue Président Kennedy");
        //THEN
        assertEquals("130 avenue Président Kennedy", util.getAdresse());
    }

    @Test
    public void testGetDateAdhesion () {
        //GIVEN
        Date dat = new Date(2022, 12, 25);
        Utilisateur util = new Utilisateur(258, "Macaux", "51 rue Blaise Pascal", dat);
        //WHEN
        //THEN
        assertEquals(dat, util.getDateAdhesion());
    }
    
   @Test
   public void testGetState () {
       //GIVEN
       Date dat = new Date(2022, 12, 25);
       Utilisateur util = new Utilisateur(258, "Macaux", "51 rue Blaise Pascal", dat);
       //WHEN
       //THEN
       assertTrue(util.getState() instanceof UtilisateurAttente);
   }

    @Test
    public void testChangeState () {
        //GIVEN
        Date dat = new Date(2022, 12, 25);
        Utilisateur util = new Utilisateur(258, "Macaux", "51 rue Blaise Pascal", dat);
        //WHEN
        util.changeState(new UtilisateurSupprime(util));
        //THEN
        assertTrue(util.getState() instanceof UtilisateurSupprime);
    }
    
    @Test
    public void testValide () throws BadStateChangeException {
        //GIVEN
        Date dat = new Date(2022, 12, 25);
        Utilisateur util = new Utilisateur(258, "Macaux", "51 rue Blaise Pascal", dat);
        //WHEN
        util.valide();
        //THEN
        assertTrue(util.getState() instanceof UtilisateurActif);
    }

    @Test
    public void testSuspend () throws BadStateChangeException {
        //GIVEN
        Date dat = new Date(2022, 12, 25);
        Utilisateur util = new Utilisateur(258, "Macaux", "51 rue Blaise Pascal", dat);
        util.changeState(new UtilisateurActif(util));
        //WHEN
        util.suspend();
        //THEN
        assertTrue(util.getState() instanceof UtilisateurSuspendu);
    }

    @Test
    public void testReactive () throws BadStateChangeException {
        //GIVEN
        Date dat = new Date(2022, 12, 25);
        Utilisateur util = new Utilisateur(258, "Macaux", "51 rue Blaise Pascal", dat);
        util.changeState(new UtilisateurSuspendu(util));
        //WHEN
        util.reactive();
        //THEN
        assertTrue(util.getState() instanceof UtilisateurActif);
    }

    @Test
    public void testSupprime () throws BadStateChangeException {
        //GIVEN
        Date dat = new Date(2022, 12, 25);
        Utilisateur util = new Utilisateur(258, "Macaux", "51 rue Blaise Pascal", dat);
        //WHEN
        util.supprime();
        //THEN
        assertTrue(util.getState() instanceof UtilisateurSupprime);
    }

}