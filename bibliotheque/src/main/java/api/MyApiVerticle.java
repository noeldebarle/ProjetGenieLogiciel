package api;

import java.sql.Date;
import java.util.List;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RequestBody;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import metiers.Utilisateur;


public class MyApiVerticle extends AbstractVerticle {
  // Quand le verticle se lance
  @Override
  public void start() throws Exception {
	  
	  // Création du routeur
	  Router router = Router.router(vertx);
	  // Définition de la route
	  router.post("/utilisateurs")
	      .handler(this::createUser);
	  // Lancement du serveur
	  vertx.createHttpServer()
	      .requestHandler(router)
	      .listen(8000);

//	  vertx.createHttpServer()
//      .requestHandler(routingContext -> routingContext.response().end("Bienvenue sur l'API de gestion de bibliothèque"))
//      .listen(8000);
	  
  }
  // Quand le verticle s'arrête
  @Override
  public void stop() throws Exception {
  }

private void createUser(RoutingContext routingContext) {
	  System.out.println(routingContext.queryParams().toString());
	  MultiMap parametres = routingContext.queryParams();
	  System.out.println(parametres.get("nom"));
	  System.out.println(parametres.get("adresse"));
//	  final String nom = routingContext.getParam("nom",null);
//	  final String adresse = body.asString("adresse");
	  final Date today = new Date(System.currentTimeMillis());
	  
//	 Utilisateur utilisateur = new Utilisateur(0, nom, adresse,today);
//	 System.out.println("création de l'utilisateur " + utilisateur.getNom());
	 
	}
}
