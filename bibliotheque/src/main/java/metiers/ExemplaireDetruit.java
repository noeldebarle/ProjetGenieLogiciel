package metiers;

public class ExemplaireDetruit  extends ExemplaireState{

    ExemplaireDetruit(Exemplaire exemplaire) {
        super(exemplaire);
    }

    public void valide() throws BadStateChangeException{
    	throw new BadStateChangeException("Impossible de valider un exemplaire détruit");
    }

    public void retourRayon() throws BadStateChangeException{
    	throw new BadStateChangeException("Impossible de remettre en rayon un exemplaire détruit");
    }

    public void detruit() throws BadStateChangeException {
    	throw new BadStateChangeException("Impossible de détruire un exemplaire déjà détruit");
    }

    public void emprunte() throws BadStateChangeException{
    	throw new BadStateChangeException("Impossible d'emprunter un exemplaire détruit");
    }
}
